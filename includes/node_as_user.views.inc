<?php

/**
 * @file
 * Node as User - Views integration
 */

/**
 * Implements hook_views_data_alter().
 */
function node_as_user_views_data() {
  $data['node_as_user']['table']['group'] = t('Node as User');
  $data['node_as_user']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['node_as_user']['nid'] = array(
    'title' => t('Node as User - Node'),
    'help' => t("Create a relationship to the node that owns a user."),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('node'),
    ),
  );
  $data['node_as_user']['uid'] = array(
    'title' => t('Node as User - User'),
    'help' => t("Create a relationship to the node's proxy user."),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('user'),
    ),
  );

  return $data;
}
