DEPENDENCIES
------------

 * node
 * user


INSTALLATION
------------

Installing the Node as User module is simple:

1) Copy the node_as_user folder to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/modules)

3) Enable and configure Node as User for the appropriate node types. The
   settings are on the node-type form at (/admin/structure/types/manage/<type-name>)
   - "Allow nodes of this type to act as a user" - turns it on
   - If you have existing content of this type, you will see an option
     to retroactively create the missing users.
   - The username can be configured using tokens. Note that random numbers will
     be appended if necessary to ensure a unique username
   - The default roles can also be configured.


USAGE
------------

Aside from managing the node/user relationship this module doesn't do much at
this point. For Views, you may make use of the provided relationships to pull
in the user or node informaiton as needed.
